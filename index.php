<?php
    require_once('Animal.php');
    require_once('Frog.php');
    require_once('Ape.php');

    $sheep = new Animal("shaun",4,"No");

    //animal
    echo "Nama Hewan : " . $sheep->nama. "<br>"; // "shaun"
    echo "Legs : " . $sheep->legs. "<br>"; // 4
    echo "Cold Blooded : " . $sheep->cold_blooded." <br>"; // "no"
    echo "<br>";
    //Frog
    $kodok = new Frog("buduk",4,"No");
    echo "Nama Hewan : " . $kodok->nama. "<br>"; // "shaun"
    echo "Legs : " . $kodok->legs. "<br>"; // 4
    echo "Cold Blooded : " . $kodok->cold_blooded. "<br>"; // "no"
    echo "Jump : ";
    echo  $kodok->jump(). "<br>"; // "hop hop"
    echo "<br>";
    //ape
    $sungokong = new Ape("kera sakti",2,"No");
    echo "Nama Hewan : " . $sungokong->nama. "<br>"; // "shaun"
    echo "Legs : " . $sungokong->legs. "<br>"; // 4
    echo "Cold Blooded : " . $sungokong->cold_blooded. "<br>"; // "no"
    echo "Yell : ";
    $sungokong->yell();  // "Auooo"


?>